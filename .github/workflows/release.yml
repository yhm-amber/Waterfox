name: 🎁 Release Workflow

on:
  workflow_dispatch:
    inputs:
      version:
        description: 'Version'
        required: true
        default: 'G'

env:
  AWS_EC2_METADATA_DISABLED: true
  AWS_REGION: us-west-2

jobs:
  soft-release:
    name: Soft Release
    runs-on: ubuntu-latest
    steps:
      - name: Download assets
        run: |
          aria2c https://cdn.waterfox.net/staging/x86_64/Windows_NT/Waterfox\ ${{ github.event.inputs.version }}\ Setup.exe
          aria2c https://cdn.waterfox.net/staging/x86_64/Windows_NT/Install\ Waterfox.exe
          aria2c https://cdn.waterfox.net/staging/x86_64/Linux/waterfox-${{ github.event.inputs.version }}.en-US.linux-x86_64.tar.bz2
          aria2c https://cdn.waterfox.net/staging/x86_64/Darwin/Waterfox\ ${{ github.event.inputs.version }}\ Setup.dmg
          aria2c https://cdn.waterfox.net/staging/aarch64/Darwin/Waterfox\ ${{ github.event.inputs.version }}\ ARM\ Setup.dmg

      - name: Release Step
        uses: ncipollo/release-action@v1
        with:
          allowUpdates: true
          # In the future, add release notes to this step.
          removeArtifacts: true
          tag: ${{ github.event.inputs.version }}
          token: ${{ secrets.GITHUB_TOKEN }}

      - name: 🪟 Upload Windows Installer
        uses: ncipollo/release-action@v1
        with:
          allowUpdates: true
          artifacts: Waterfox\ ${{ github.event.inputs.version }}\ Setup.exe
          artifactContentType: application/vnd.microsoft.portable-executable
          tag: ${{ github.event.inputs.version }}
          token: ${{ secrets.GITHUB_TOKEN }}
      - name: 🪟 Upload Windows Stub
        uses: ncipollo/release-action@v1
        with:
          allowUpdates: true
          artifacts: Install\ Waterfox.exe
          artifactContentType: application/vnd.microsoft.portable-executable
          tag: ${{ github.event.inputs.version }}
          token: ${{ secrets.GITHUB_TOKEN }}
      - name: 🍎 Upload Apple X64 Image
        uses: ncipollo/release-action@v1
        with:
          allowUpdates: true
          artifacts: Waterfox\ ${{ github.event.inputs.version }}\ Setup.dmg
          artifactContentType: application/x-apple-diskimage
          tag: ${{ github.event.inputs.version }}
          token: ${{ secrets.GITHUB_TOKEN }}
      - name: 🍎 Upload Apple ARM Image
        uses: ncipollo/release-action@v1
        with:
          allowUpdates: true
          artifacts: Waterfox\ ${{ github.event.inputs.version }}\ ARM\ Setup.dmg
          artifactContentType: application/x-apple-diskimage
          tag: ${{ github.event.inputs.version }}
          token: ${{ secrets.GITHUB_TOKEN }}
      - name: 🐧 Upload Linux tarball
        uses: ncipollo/release-action@v1
        with:
          allowUpdates: true
          artifacts: waterfox-${{ github.event.inputs.version }}.en-US.linux-x86_64.tar.bz2
          artifactContentType: application/zip
          tag: ${{ github.event.inputs.version }}
          token: ${{ secrets.GITHUB_TOKEN }}

      - name: 🪪 Configure AWS credentials
        uses: aws-actions/configure-aws-credentials@v1
        with:
          role-to-assume: arn:aws:iam::364189071156:role/github_actions_assume_role
          aws-region: us-west-2

      - name: ⚙︎ Dry run AUS
        run: |
          os=(WINNT_x86_64 Linux_x86_64 Darwin_x86_64 Darwin_aarch64)
          for j in ${os[@]}
          do
            update_loc="s3://aus.waterfox.net/update/staging/"$j"/update.xml"
            mkdir -p aus_tmp/$j/
            aws s3 cp --content-type="text/xml" --metadata-directive="REPLACE" $update_loc \
              ./aus_tmp/$j/update.xml
            sed -i 's/staging\/x86_64\/Linux/releases\/linux64\/update/g' ./aus_tmp/$j/update.xml
            sed -i 's/staging\/x86_64\/Windows_NT/releases\/win64\/update/g' ./aus_tmp/$j/update.xml
            sed -i 's/staging\/x86_64\/Darwin/releases\/osx64\/update/g' ./aus_tmp/$j/update.xml
            sed -i 's/staging\/aarch64\/Darwin/releases\/osx64\/update/g' ./aus_tmp/$j/update.xml
            aws s3 cp --content-type="text/xml" --metadata-directive="REPLACE" ./aus_tmp/$j/update.xml \
              s3://aus.waterfox.net/update/Waterfox/tmp/$j/update.xml
          done

          for i in $(aws s3 ls s3://aus.waterfox.net/update/Waterfox/ | awk '{OFS=" "};{if ($1=="PRE") print $2}')
          do
              if [[ $i != '/' ]]
              then
              ver=$(echo $i | sed 's/\///g')
                  if [[ $i == "G3"* ]]
                  then
                      os=(Windows_NT Linux Darwin)
                      for j in ${os[@]}
                      do
                          dest=s3://aus.waterfox.net/update/Waterfox/$ver/default/$j/update.xml
                          if [[ $j == "Windows_NT" ]]
                          then
                            j=WINNT
                          fi
                          aws s3 cp --dryrun --content-type="text/xml" --metadata-directive="REPLACE" \
                            s3://aus.waterfox.net/update/Waterfox/tmp/${j}_x86_64/update.xml \
                            $dest
                      done
                  elif [[ $i == "G4"* ]]
                  then
                      os=(WINNT_x86_64 Linux_x86_64 Darwin_x86_64 Darwin_aarch64)
                      for j in ${os[@]}
                      do
                          aws s3 cp --dryrun --content-type="text/xml" --metadata-directive="REPLACE" \
                            s3://aus.waterfox.net/update/Waterfox/tmp/$j/update.xml \
                            s3://aus.waterfox.net/update/Waterfox/$ver/default/$j/update.xml
                      done
                  fi
              fi
          done

  hard-release:
    name: Full Release
    needs: [soft-release]
    environment: hard-release
    runs-on: ubuntu-latest
    steps:
      - name: 🪪 Configure AWS credentials
        uses: aws-actions/configure-aws-credentials@v1
        with:
          role-to-assume: arn:aws:iam::364189071156:role/github_actions_assume_role
          aws-region: us-west-2

      - name: ⚙︎ Release AUS
        run: |
          os=(WINNT_x86_64 Linux_x86_64 Darwin_x86_64 Darwin_aarch64)
          for j in ${os[@]}
          do
            update_loc="s3://aus.waterfox.net/update/staging/"$j"/update.xml"
            mkdir -p aus_tmp/$j/
            aws s3 cp --content-type="text/xml" --metadata-directive="REPLACE" $update_loc \
              ./aus_tmp/$j/update.xml
            sed -i 's/staging\/x86_64\/Linux/releases\/linux64\/update/g' ./aus_tmp/$j/update.xml
            sed -i 's/staging\/x86_64\/Windows_NT/releases\/win64\/update/g' ./aus_tmp/$j/update.xml
            sed -i 's/staging\/x86_64\/Darwin/releases\/osx64\/update/g' ./aus_tmp/$j/update.xml
            sed -i 's/staging\/aarch64\/Darwin/releases\/osx64\/update/g' ./aus_tmp/$j/update.xml
            aws s3 cp --content-type="text/xml" --metadata-directive="REPLACE" ./aus_tmp/$j/update.xml \
              s3://aus.waterfox.net/update/Waterfox/tmp/$j/update.xml
          done

          for i in $(aws s3 ls s3://aus.waterfox.net/update/Waterfox/ | awk '{OFS=" "};{if ($1=="PRE") print $2}')
          do
              if [[ $i != '/' ]]
              then
              ver=$(echo $i | sed 's/\///g')
                  if [[ $i == "G3"* ]]
                  then
                      os=(Windows_NT Linux Darwin)
                      for j in ${os[@]}
                      do
                          dest=s3://aus.waterfox.net/update/Waterfox/$ver/default/$j/update.xml
                          if [[ $j == "Windows_NT" ]]
                          then
                            j=WINNT
                          fi
                          aws s3 cp --content-type="text/xml" --metadata-directive="REPLACE" \
                            s3://aus.waterfox.net/update/Waterfox/tmp/${j}_x86_64/update.xml \
                            $dest
                      done
                  elif [[ $i == "G4"* ]]
                  then
                      os=(WINNT_x86_64 Linux_x86_64 Darwin_x86_64 Darwin_aarch64)
                      for j in ${os[@]}
                      do
                          aws s3 cp --content-type="text/xml" --metadata-directive="REPLACE" \
                            s3://aus.waterfox.net/update/Waterfox/tmp/$j/update.xml \
                            s3://aus.waterfox.net/update/Waterfox/$ver/default/$j/update.xml
                      done
                  fi
              fi
          done
          touch blank.xml
          echo '<?xml version="1.0"?><updates></updates>' > blank.xml
          os=(WINNT_x86_64 Linux_x86_64 Darwin_x86_64 Darwin_aarch64)
          for i in ${os[@]}
          do
              aws s3 cp --content-type="text/xml" --metadata-directive="REPLACE" blank.xml \
                  s3://aus.waterfox.net/update/Waterfox/${{ github.event.inputs.version }}/default/$i/update.xml
          done

      - name: ⎆ Move from staging
        run: |
          ftp -n -v ftp.keycdn.com << EOT
          ascii
          user ${{ secrets.FTP_USR }} ${{ secrets.FTP_PASS }}
          prompt
            rename staging/x86_64/Windows_NT/Install\ Waterfox.exe releases/win64/installer/Install\ Waterfox.exe
            rename staging/x86_64/Windows_NT/Waterfox\ ${{ github.event.inputs.version }}\ Setup.exe releases/win64/installer/Waterfox\ ${{ github.event.inputs.version }}\ Setup.exe
            rename staging/x86_64/Windows_NT/waterfox-${{ github.event.inputs.version }}.en-US.win64.complete.xz.mar releases/win64/update/waterfox-${{ github.event.inputs.version }}.en-US.win64.complete.xz.mar
            rename staging/x86_64/Linux/waterfox-${{ github.event.inputs.version }}.en-US.linux-x86_64.tar.bz2 releases/linux64/installer/waterfox-${{ github.event.inputs.version }}.en-US.linux-x86_64.tar.bz2
            rename staging/x86_64/Linux/waterfox-${{ github.event.inputs.version }}.en-US.linux64.complete.xz.mar releases/linux64/update/waterfox-${{ github.event.inputs.version }}.en-US.linux64.complete.xz.mar
            rename staging/x86_64/Darwin/waterfox-${{ github.event.inputs.version }}.en-US.osx64.complete.xz.mar releases/osx64/update/waterfox-${{ github.event.inputs.version }}.en-US.osx64.complete.xz.mar
            rename staging/aarch64/Darwin/waterfox-${{ github.event.inputs.version }}.en-US.osx-arm64.complete.xz.mar releases/osx64/update/waterfox-${{ github.event.inputs.version }}.en-US.osx-arm64.complete.xz.mar
          EOT

      - name: ⎆ Update latest redirect
        run: |
            curl "https://api.cloudflare.com/client/v4/accounts/${{ secrets.CF_ZONE_ID }}/rules/lists/8eefc8eca6e6407e80132f90c00ce149/items" \
            -H "X-Auth-Email: ${{ secrets.CF_EMAIL }}" \
            -H "X-Auth-Key: ${{ secrets.CF_AUTH }}" \
            -H "Content-Type: application/json" \
            -d '[
            {
                "redirect": {
                "source_url": "cdn.waterfox.net/releases/win64/latest",
                "target_url": "https://cdn.waterfox.net/releases/win64/installer/Waterfox%20${{ github.event.inputs.version }}%20Setup.exe",
                "status_code": 302
                }
            }
            ]'